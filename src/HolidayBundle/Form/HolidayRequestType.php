<?php

namespace HolidayBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use HolidayBundle\Entity\Person;
use HolidayBundle\Repository\PersonRepository;

class HolidayRequestType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder->add('dateDebut',DateType::class)
                 ->add('dateFin',DateType::class)

                ->add('typeConge',ChoiceType::class, array(
                 'choices' => array('Congé' =>'Congé','Congé Parental' =>'Congé Parental',
                     'Sans solde' => 'Sans solde', 'Activités Judiciaires' => 'Activités Judiciaires', 'Autres' => 'Autres')
          ))
                ->add('dateDemande',DateType::class)
             //   ->add('dateValidation',DateType::class)
             // ->add('managerID')
                ->add ('person', EntityType::class, [
                       'class' => Person::class,
                       'query_builder' => function (PersonRepository $er){
                           return $er->createQueryBuilder('p')->orderBy ('p.id','ASC');
                       },
                       'choice_label' => function ($x){
                           return strtoupper($x->getNom());
                       }
       ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HolidayBundle\Entity\HolidayRequest'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'holidaybundle_holidayrequest';
    }


}
