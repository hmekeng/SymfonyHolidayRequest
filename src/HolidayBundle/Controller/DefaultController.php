<?php

namespace HolidayBundle\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
     /**
     * @Route("/", name="homepage")
     */
    public function accueilAction()
    {
         //dump ("hello");
                //die();
                 $objetAuthentification = $this->get('security.authentication_utils'); 
                 $errors = $objetAuthentification->getLastAuthenticationError();
                 $login = $objetAuthentification->getLastUsername();

                 $vars = ['errors' => $errors,
                    'login' => $login];

                return $this->render ('HolidayBundleViews/Default/index.html.twig',$vars); 
        
        
        //return $this->render('HolidayBundleViews/Default/index.html.twig');
    }
    /**
     * @Route("/Default/adminPage", name="adminPage")
     */
      public function adminPageAction()
    {


        return $this->render('HolidayBundleViews/Default/template.html.twig');
    }
}
