<?php

namespace HolidayBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use HolidayBundle\Form\HolidayRequestType;
use HolidayBundle\Entity\HolidayRequest;

class HolidayRequestController extends Controller
{
    // insert holidays request in the database 
     public function addHolidayRequestAction(Request $req)
      {
        $holidayRequest=new HolidayRequest();

        $formulaire= $this->createForm(HolidayRequestType::class, $holidayRequest, array(
           'method'=>'POST',
           'action'=>$this->generateUrl('addHolidayRequest')
                ));
        $formulaire->handleRequest($req);

        if ($formulaire->isSubmitted() && $formulaire->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($holidayRequest);
            $em->flush();
             //redirect vers la page liste des resquest qui sera visualisé par l'admin
            return $this->redirectToRoute('successPage');


        }
        else{

            return $this->render("HolidayBundleViews/HolidayRequestControllerViews/addholidayRequest.html.twig",
                array ('formulaire'=>$formulaire->createView()));

        }

    }
     // success page after adding a holiday request
     public function successPageAction()       
    {
        return $this->render("HolidayBundleViews/HolidayRequestControllerViews/success_page.html.twig");
    }
    
    //retrieve all holiday request from the database 
     public function showAllHolidayRequestAction ()
    {
        $em=$this->getDoctrine()->getManager();
      
        $holidayRequests=$em->getRepository('HolidayBundle:HolidayRequest')->findAll();
         return $this->render("HolidayBundleViews/HolidayRequestControllerViews/showAllHolidayRequest.html.twig", array('HolidayList'=>$holidayRequests));
    }
    
    
    
// Admin Dashboad with all holidays by person we suppose that one person can ask many holidays we therefore have a list of holidays

    public function listOfHolidayByPersonAction(Request $request)
     {

         $id =$request->get('id');
         $holidays = $this->getDoctrine()->getRepository("HolidayBundle:HolidayRequest")->findBy(array('person' => $id));
         return $this->render('HolidayBundleViews/HolidayRequestControllerViews/listOfAllHolidays.html.twig', array('holidays' => $holidays, 'id' => $id));
     }
    
  
    
    
}