<?php

namespace HolidayBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use HolidayBundle\Form\PersonType;
use HolidayBundle\Entity\Person;


class PersonController extends Controller
{
    
    public function addPersonAction(Request $req)
    {

    //important pour notre formulaire
        $passwordEncoder=$this->get('security.password_encoder');

        $person=new Person();

         $formulaire = $this->createForm(PersonType::class, $person);
         // ici on commence le traitement du formulaire
         $formulaire->handleRequest($req);

         if ($formulaire->isSubmitted() && $formulaire->isValid()){


         $password = $passwordEncoder->encodePassword($person, $person->getPasswordOriginal());

         $person->setPassword($password);

        $person ->setPassword($password);
        // encoder person dans la Database
        $em = $this->getDoctrine()->getManager();
        $em->persist($person);
        $em->flush();
         //redirect vers la page connexion
        return $this->redirectToRoute('accueil');


        }
        else
        {
                return $this->render
               ('HolidayBundleViews/PersonControllerViews/addPerson.html.twig',
                    array ('formulaire'=>$formulaire->createView()));
        }
      }

      // the connexion action is put in Defaut controller
       // public function connexionPageAction(Request $req){
               //dump ("hello");
                //die();
           //      $objetAuthentification = $this->get('security.authentication_utils'); 
            //     $errors = $objetAuthentification->getLastAuthenticationError();
              //   $login = $objetAuthentification->getLastUsername();

               //  $vars = ['errors' => $errors,
               //     'login' => $login];

              //  return $this->render ('HolidayBundleViews/PersonControllerViews/connexionPage.html.twig',$vars);
                
                
           // }
            
             public function successConnectAction(){

              return $this->render("HolidayBundleViews/PersonControllerViews/success_connect.html.twig");
          }
            

        // cette action se trouve dans app/config/security.yml
              public function verifieUserAction(){

              if($this->getUser()->getEmail()=="admin@gmail.com")
              {

               return $this->redirectToRoute ("adminPage");
              }
              else
              {

               return $this->redirectToRoute ("addHolidayRequest");
              }
          }
    
}

