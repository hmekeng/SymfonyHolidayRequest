<?php

namespace HolidayBundle\Entity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Person
 */
class Person implements UserInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $typeCompte;

    /**
     * @var \DateTime
     */
    private $dateConnexion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Person
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Person
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Person
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Person
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set typeCompte
     *
     * @param string $typeCompte
     *
     * @return Person
     */
    public function setTypeCompte($typeCompte)
    {
        $this->typeCompte = $typeCompte;

        return $this;
    }

    /**
     * Get typeCompte
     *
     * @return string
     */
    public function getTypeCompte()
    {
        return $this->typeCompte;
    }

    /**
     * Set dateConnexion
     *
     * @param \DateTime $dateConnexion
     *
     * @return Person
     */
    public function setDateConnexion($dateConnexion)
    {
        $this->dateConnexion = $dateConnexion;

        return $this;
    }

    /**
     * Get dateConnexion
     *
     * @return \DateTime
     */
    public function getDateConnexion()
    {
        return $this->dateConnexion;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $holidayRequests;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->holidayRequests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add holidayRequest
     *
     * @param \HolidayBundle\Entity\HolidayRequest $holidayRequest
     *
     * @return Person
     */
    public function addHolidayRequest(\HolidayBundle\Entity\HolidayRequest $holidayRequest)
    {
        $this->holidayRequests[] = $holidayRequest;

        return $this;
    }

    /**
     * Remove holidayRequest
     *
     * @param \HolidayBundle\Entity\HolidayRequest $holidayRequest
     */
    public function removeHolidayRequest(\HolidayBundle\Entity\HolidayRequest $holidayRequest)
    {
        $this->holidayRequests->removeElement($holidayRequest);
    }

    /**
     * Get holidayRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHolidayRequests()
    {
        return $this->holidayRequests;
    }
    
         // propriété  rajouté pour $passwordOriginal ()
    private $passwordOriginal;

    function getPasswordOriginal() {
        return $this->passwordOriginal;
    }

    function setPasswordOriginal($passwordOriginal) {
        $this->passwordOriginal = $passwordOriginal;
    }

    public function eraseCredentials() {
        
    }

    public function getRoles() {
      return array ('ROLE_USER');  
    }

    public function getSalt() { // ceci permet d'ajouter plus de hash sur le passeword
       return null; 
    }

    public function getUsername() {
     return $this->email; //ici on reconnait notre   user par l'email ça pourait etre autre chose login (return $this->login;)   
    }
    
    
}
